package com.lidka;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Map<String, String> attachments = new HashMap<>();
        attachments.put("attachment1", "bla.txt");
        attachments.put("attachment2", "bla2.txt");
        attachments.put("attachment3", "bla3.txt");
        attachments.put("attachment4", "bla4.txt");

        List<Attachment> attachmentsList = attachments.entrySet().stream()
                .map(attachment -> new Attachment(attachment.getKey(), attachment.getValue()))
                .collect(Collectors.toList());
        System.out.println("Pierwsza metoda:");
        System.out.println(attachmentsList);

        List<Attachment> attachmentsList2 = attachments.entrySet().stream()
                .map(Main::convert) // gdyby convert bylo metoda niestatyczna, to byloby: this::convert, ale musi byc statyczna, bo robie to mainie, ktory jest tez statyczny
                .collect(Collectors.toList());
        System.out.println("Druga metoda:");
        System.out.println(attachmentsList2);

        Map<String, String> result = attachmentsList.stream().collect(Collectors.toMap(Attachment::getAttachmentName, Attachment::getFilename));
        System.out.println("W druga strone:");
        System.out.println(result);
    }

    public static Attachment convert(Map.Entry<String, String> attachment) {
        return new Attachment(attachment.getKey(), attachment.getValue());
    }
}

class Attachment {
    private String filename;
    private String attachmentName;
    private String type;

    public Attachment(String attachmentName, String filename) {
        this.filename = filename;
        this.attachmentName = attachmentName;
        this.type = "mime/whatever";
    }

    public String getFilename() {
        return filename;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "filename='" + filename + '\'' +
                ", attachmentName='" + attachmentName + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}